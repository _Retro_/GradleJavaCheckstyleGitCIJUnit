import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

public class HelloGitLabTest extends TestCase {
    ArrayList<Integer> paramTest;

    int a = 3;
    int b = 4;
    int c = 0;

    public HelloGitLabTest()
    {
        //Si generano 1000 possibili casi randomici per testare addition()
        this.paramTest = new ArrayList<Integer>(); int i;
        Random rnd = new Random();
        for(i=0;i<1000;i++) {
            int a = rnd.nextInt();
            int b = rnd.nextInt();
            c = a + b + 1;
            paramTest.add(a);paramTest.add(b);paramTest.add(c);

        }
    }

    //La funzione testAddition viene testata con 1000 somme randomiche
    @Test
    public void testAddition() {
        HelloGitLab h = new HelloGitLab();
        int i = 0,a=0,b=0,c=0,result=0;
        for(int t : this.paramTest)
        {
            switch(i)
            {
                case 0:
                    a = t; i++;
                    break;
                case 1:
                    b = t; i++;
                    break;
                case 2:
                    c = t;
                    System.out.println("addition: Testata con a="+a+" b="+b+" c="+c);
                    result = h.addition(a,b);
                    assertEquals(c,result);
                    i=0;
                    break;
            }
        }
    }
}